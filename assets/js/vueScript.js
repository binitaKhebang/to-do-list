new Vue({
	el: '#vueToDo',
	data: function() {
		return {
			date: this.getDate(),
			time: this.getTime(),
			tasksArray: [],
			task: '',
			toggleLabel: 1,
			toggleInput: 0,
			editing: {},
			currentDate: ''
		}
	},
	methods: {
		getDate: function() {
			this.currentDate = new Date();
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
			var today = months[this.currentDate.getMonth()] + ' ' + this.currentDate.getDate() + this.suffix(this.currentDate.getDate()) + ', ' + days[this.currentDate.getDay()];
			return today;
		},
		suffix: function(d) {
			if(d > 3 && d < 21) {
				return 'th';
			}else if(d % 10 == 1) {
				return 'st';
			}else if(d % 10 == 2) {
				return 'nd';
			}else if(d % 10 == 3) {
				return 'rd';
			}else {
				return 'th';
			}
		},
		getTime: function() {
			return this.currentDate.getHours() + ':' + this.currentDate.getMinutes() + ' ' + (this.currentDate.getHours() >= 12 ? 'PM':'AM');
		},
		addTask: function() {
			if(localStorage.getItem('Tasks') == null) {
		     	this.tasksArray = [];
			}else {
			    this.tasksArray = this.jsonStObj(localStorage.getItem('Tasks'));
			}
			this.tasksArray.push({
				id: this.tasksArray.length,
				todo: this.task,
				completed: false,
				createdOn: Date.now()
			});
			localStorage.setItem('Tasks', this.jsonStObj(this.tasksArray));
			this.task = '';
		},
		showTasks: function() {
		    this.tasksArray =  this.jsonStObj(localStorage.getItem('Tasks'));
		},
		editTask: function(id) {
			this.$set(this.editing, id, true);
			this.toggleLabel = 0;
			this.toggleInput = 1;
			this.$nextTick(() => {
				this.$refs["todoRef"+id][0].focus();
	      	})
		},
		updateTask: function(taskId) {
			this.tasksArray.forEach(function(task,index) {
				if(task.id == taskId) {
					task.todo = event.target.value;;
				}
			});
			localStorage.setItem('Tasks', this.jsonStObj(this.tasksArray));
			if(event.key == 'Enter') {
				this.$set(this.editing, taskId, false);
				this.toggleLabel = 1;
				this.toggleInput = 0;
			}
		},
		deleteTask: function(index) {
			this.tasksArray.splice(index,1);
			localStorage.setItem('Tasks', this.jsonStObj(this.tasksArray));
		},
		markAsCompleted: function(index) {
			this.tasksArray[index].completed = !this.tasksArray[index].completed;
			localStorage.setItem('Tasks', this.jsonStObj(this.tasksArray));
		},
		getCreatedTime: function(taskCreatedOn) {
			var timeDifference = Date.now() - taskCreatedOn;
		    var seconds = Math.floor((timeDifference / 1000) % 60);
		    var minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
		    var hours = Math.floor((timeDifference / (1000 * 60 * 60)) % 24);
		    var days = Math.floor(hours / 24);
		    if(minutes == 1 && hours < 1) {
		    	return ' a minute ago';
		    }else if(minutes < 1 && hours < 1) {
		    	return ' a few seconds ago';
		    }else if(minutes > 1 && hours < 1) {
		    	return minutes + ' minutes ago';
		    }else if(hours == 1) {
		    	return ' an hour ago';
		    }else if(hours > 1) {
		    	return hours + ' hours ago';
		    }else if(days == 1) {
		    	return '1 day ago';
		    }else if(days > 1) {
		    	return days + ' days ago';
		    }
		},
		/* function to convert JSON string to object and object to string */
		jsonStObj: function(strObj) {
			if(typeof(strObj) == "string") {
				return JSON.parse(strObj);
			}if(typeof(strObj) == "object") {
				return JSON.stringify(strObj);
			}
		}
	},
	mounted: function (){
		this.showTasks();
	}
});