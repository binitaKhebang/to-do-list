if(localStorage.getItem('Tasks') == null){
    var tasks =[];
}else{
    tasks = jsonStObj(localStorage.getItem('Tasks'));
}
var date = new Date();
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
document.getElementById("date").innerHTML = months[date.getMonth()] + ' ' + date.getDate() + suffix(date.getDate()) + ', ' + days[date.getDay()];
document.getElementById("time").innerHTML = date.getHours() + ':' + date.getMinutes() + ' ' + (date.getHours() >= 12 ? 'PM':'AM');
listTasks();

function suffix(d) {
	if(d > 3 && d < 21) {
		return 'th';
	}else if(d % 10 == 1) {
		return 'st';
	}else if(d % 10 == 2) {
		return 'nd';
	}else if(d % 10 == 3) {
		return 'rd';
	}else {
		return 'th';
	}
}

function addTask() {
    if(event.key === 'Enter') {
	 	var newTask = document.getElementById("task").value;
	 	tasks.push({
	 		id: tasks.length,
	 		todo: newTask,
	 		completed: false,
	 		createdOn: Date.now()
	 	});
	 	localStorage.setItem('Tasks', jsonStObj(tasks));
        listTasks();
        document.getElementById("task").value = '';
    }
}

function listTasks() {
	var data = jsonStObj(localStorage.getItem('Tasks'));
	let list = data.map(function(taskDetail, index) {
		var createdTime = getCreatedTime(taskDetail.createdOn);
		if(taskDetail.completed == false) {
			return `<li id="task_${taskDetail.id}" class="to_do"><input type="checkbox" id="checked${taskDetail.id}" onclick="markAsCompleted(${index})"/><label for="checked${taskDetail.id}" class="todo_check"> ${taskDetail.todo} </label><button class="edit" onclick="editTask(${taskDetail.id})">edit</button><button class="delete" onclick="deleteTask(${index})">x</button><span class="time">${createdTime}</span></li>`;
		}else {
			return `<li id="task_${taskDetail.id}" class="to_do"><input type="checkbox" id="checked${taskDetail.id}" onclick="markAsCompleted(${index})" checked/><label for="checked${taskDetail.id}" class="todo_check"> ${taskDetail.todo} </label><button class="delete" onclick="deleteTask(${index})">x</button><span class="time">${createdTime}</span></li>`;
		}
	}).join('');
    document.getElementById("tasks").innerHTML = list;
}

function deleteTask(taskIndex) {
	tasks.splice(taskIndex,1);
	localStorage.setItem('Tasks', jsonStObj(tasks));
    listTasks();
}

function markAsCompleted(index) {
	tasks[index].completed = !tasks[index].completed;
	localStorage.setItem('Tasks', jsonStObj(tasks));
	listTasks();
}

function editTask(taskId) {
	tasks.forEach(function(task, index) {
		if(taskId == task.id) {
			document.getElementById("task_"+taskId).innerHTML = `<input type="checkbox" id="checked${task.id}"/><input id="updatedTask" type="text" value="${task.todo}" onkeydown="updateTask(${task.id})"/>`;
			document.getElementById("updatedTask").focus();
		}
	});
}

function updateTask(taskId) {
	if(event.key == 'Enter') {
		tasks.forEach(function(task, index) {
			if(task.id == taskId) {
				task.todo = event.target.value;
			}
			localStorage.setItem('Tasks', jsonStObj(tasks));
			listTasks();
		});
	}
}

function getCreatedTime(taskCreatedOn) {
	var timeDifference = Date.now() - taskCreatedOn;
    var seconds = Math.floor((timeDifference / 1000) % 60);
    var minutes = Math.floor((timeDifference / (1000 * 60)) % 60);
    var hours = Math.floor((timeDifference / (1000 * 60 * 60)) % 24);
    var days = Math.floor(hours / 24);
    if(minutes == 1 && hours < 1) {
    	return ' a minute ago';
    }else if(minutes < 1 && hours < 1) {
    	return ' a few seconds ago';
    }else if(minutes > 1 && hours < 1) {
    	return minutes + ' minutes ago';
    }else if(hours == 1) {
    	return ' an hour ago';
    }else if(hours > 1) {
    	return hours + ' hours ago';
    }else if(days == 1) {
    	return ' 1 day ago';
    }else if(days > 1) {
    	return days + ' days ago';
    }
}

/* function to convert JSON string to object and object to string */
function jsonStObj(strObj) {
	if(typeof(strObj) == "string") {
		return JSON.parse(strObj);
	}if(typeof(strObj) == "object") {
		return JSON.stringify(strObj);
	}
}